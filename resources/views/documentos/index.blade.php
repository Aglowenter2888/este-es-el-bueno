
@extends('layouts.app')

@section('Titulo', 'Repositorio de investigaciones ')

@section('content')

  <!-- Bootstrap CSS -->
  <div class="panel-header colorut" >
		<div class="page-inner py-5">
			<div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
			    <div>
			        <h2 class="text-black pb-2 fw-bold">Documentos realizados</h2>
		        </div>		
	        </div>
		</div>
    </div>
	<div class="page-inner mt--5">	
  <h1>Buscar Por año</h1>				
  <div class="btn-group btn-group-toggle" >
        <?php $fecha = date("Y");?>
        <!--Actualmente--->
          <form action="/fechaActual" method="GET" class="form-inline">
            <button type="submit" name="fechaActual" class="btn btn-default" value="<?php echo $fecha;?>" >Actual</button>
          </form>
          <!--Primer año--->
          <form action="/pasado" method="GET" class="form-inline">
            <button type="submit" name="pasado" class="btn btn-default" value="<?php echo $fecha-1;?>">1 año</button>
          </form>
            <!--Tercer año--->
          <form action="/antiguo" method="GET" class="form-inline">
            <button type="submit" name="antiguo" class="btn btn-default" value="<?php echo $fecha-3;?>">3 años</button>
          </form>
          </div>
    <div class="container">
        <div class="table-wrapper">
            <div class="table-title">
                <div class="row">
                    <div class="col-sm-6">
                          <table class="table table-striped">

          <tr>
              <th>#</th>
              <th>Titulo</th>
              <th>Autor</th>
              <th>Descripcion</th>
              <th>Ver</th>
              <th>Descargar</th>
              <th>Fecha de Creacion</th>
              <th>Eliminar</th>
            </tr>
                @foreach ($file as $key=>$data)
                <tr>
                    <td>{{++$key}}</td>
                    <td>{{$data->titulo}}</td>
                    <td>{{$data->autor}}</td>
                    <td>{{$data->descripcion}}</td>
                    <td><a class="btn btn-success" href="/files/{{$data->id}}">ver</a></td>
                    <td><a class="btn btn-success" href="/file/download/{{$data->file}}">Descargar</a> </td>
                    <td>{{$data->created_at}}</td>
                    <td>
                        <form action="{{route("archivos.destroy", $data->id)}}" method="POST"> 
                        @method("DELETE")
                        @csrf
                       <input type="submit" class="btn btn-danger" value="Eliminar">
                       </form>
                    </td>
                    </tr>
                @endforeach
      </table>
      @endsection